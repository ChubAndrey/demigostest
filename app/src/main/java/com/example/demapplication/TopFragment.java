package com.example.demapplication;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by Андрей on 16.01.2016.
 */
public class TopFragment extends Fragment {

    View view;
    LayoutInflater inflat;

    List<JSONData> topDatas;
    ListView listView;
    TopListAdapter adapter;

    public TopFragment () {
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_one, container, false);
        listView = (ListView)view.findViewById(R.id.list_view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new ProgressTask().execute();
    }

    class ProgressTask extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... path) {

            String content;
            try {

                content = getContent("http://demigos.com/test/releases.html");
            } catch (IOException ex) {
                content = ex.getMessage();
            }

            return content;
        }

        @Override
        protected void onProgressUpdate(Void... items) {

        }

        @Override
        protected void onPreExecute() {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(view.getContext());
                progressDialog.setMessage("Synchronizing, please wait...");
                progressDialog.show();
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setCancelable(false);
            }
        }

        @Override
        protected void onPostExecute(String content) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            Gson gson = new Gson();
            JSONItemsArray topObjct = gson.fromJson(content, JSONItemsArray.class);
            List<JSONData> topDatas = topObjct.JSONItems;

            adapter = new TopListAdapter(view.getContext(),R.layout.listview_item_top, topDatas);
            listView.setAdapter(adapter);

        }

        private String getContent(String path) throws IOException {
            BufferedReader reader = null;
            try {
                URL url = new URL(path);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setReadTimeout(0);
                c.connect();
                reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
                StringBuilder buf = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    buf.append(line + "\n");
                }
                return (buf.toString());
            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
        }
    }

}
