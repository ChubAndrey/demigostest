package com.example.demapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Андрей on 17.01.2016.
 */
public class TopListAdapter extends ArrayAdapter<JSONData> {

    private List<JSONData> items;
    private int layoutResourceId;
    private Context context;

    public TopListAdapter(Context context, int layoutResourceId, List<JSONData> items) {
        super(context, layoutResourceId, items);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.items = items;

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public JSONData getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        ViewHolder holder;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.listview_item_top, parent, false);
            holder = new ViewHolder();
            holder.titleView = (TextView) view.findViewById(R.id.title);
            holder.dateView = (TextView) view.findViewById(R.id.date);
            holder.authorView = (TextView) view.findViewById(R.id.author);
            holder.descriptionView = (TextView) view.findViewById(R.id.description);
            holder.imageView = (ImageView)view.findViewById(R.id.im_view);
            view.setTag(holder);
        }
        else {

            holder = (ViewHolder) view.getTag();
        }

        final JSONData topData = items.get(index);

        holder.titleView.setText(topData.getTitle());
        holder.dateView.setText(topData.getDate());
        holder.authorView.setText(topData.getAuthor());
        holder.descriptionView.setText(topData.getDescription());
        new ImageLoader(holder.imageView).execute(topData.getImage());

        holder.authorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(parent.getContext(), "button clicked: " + topData.getAuthor(), Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    static class ViewHolder{

        TextView titleView;
        TextView dateView;
        TextView authorView;
        TextView descriptionView;
        ImageView imageView;

    }

}
