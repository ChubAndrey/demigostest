package com.example.demapplication;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Андрей on 17.01.2016.
 */
public class JSONData {

    @SerializedName("title")
    public String title;

    @SerializedName("date")
    public String date;

    @SerializedName("author")
    public String author;

    @SerializedName("description")
    public String description;

    @SerializedName("image")
    public String image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
