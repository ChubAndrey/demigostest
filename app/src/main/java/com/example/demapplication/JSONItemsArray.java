package com.example.demapplication;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Андрей on 16.01.2016.
 */
public class JSONItemsArray {

    @SerializedName("data")
    public List<JSONData> JSONItems = new ArrayList<JSONData>();

    @SerializedName("status")
    public String status;

}
